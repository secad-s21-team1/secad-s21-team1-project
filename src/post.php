<?php
	require 'database.php';
	require 'session_auth.php';
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}

	$postid = sanitize_input($_POST["postid"]);

	// check if the post ID is valid
	if(!isValidPost($postid)) {
		echo "<script>alert('This post does not exist. Please enter a valid post ID.');</script>";
		header("Refresh:0; url=mainpage.php");
	}

	getPost($_POST["postid"]);
	echo "Comment Section: \r\n"; // what is \r\n?
	echo "<br><br>";
	showComments($_POST["postid"]);


	if(strcmp(getOwnerOfPost($postid), $_SESSION["username"]) == 0) {
?>
		<form action="editpost.php" method="POST">
	        Edit your post in the box below <br>
	        <input type="hidden" name="postid" value="<?php echo  $postid; ?>" />
	        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
	        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
	                    pattern=".{1,250}"
	                    title="The post must have between 1 and 250 characters" /> <br>
	       	<button class="button" type="submit">
	           Edit Post
	        </button>
	    </form>
<?php
	}
?>

	<form action="addcomment.php" method="POST">
        Write a comment in the box below <br>
        <input type="hidden" name="parentpostid" value="<?php echo  $postid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
                    pattern=".{1,250}"
                    title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Comment
        </button>
    </form>


<?php
	// user can delete post if he is the owner or a superuser
	if(strcmp(getOwnerOfPost($postid), $_SESSION["username"]) == 0) {
?>
    <form action="deletepost.php" method="POST">
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
       	<button class="button" type="submit">
           Delete Post
        </button>
    </form>
<?php
	}
?>

<a href="mainpage.php">Main Page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php
	function getPost($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL; $content = NULL;
		if(!$stmt->bind_result($owner, $content)) echo "Binding failed";
		while($stmt->fetch()){
			echo "<br>Post by " . htmlentities($owner) . ": " . htmlentities($content) . "<br><br><br>";
		}
	}

	function showComments($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content, commentID FROM comments WHERE parentPostID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL; $content = NULL; $commentID = NULL;
		if(!$stmt->bind_result($owner, $content, $commentID)) echo "Binding failed";
		while($stmt->fetch()){
			echo htmlentities($owner) . " with comment ID " . htmlentities($commentID) . ": " . htmlentities($content) . "<br><br>";
			if (strcmp($owner, $_SESSION['username']) == 0){

	?>
    		<form action="comment.php" method="POST">
        		<input type="hidden" name="commentid" value="<?php echo $commentID; ?>" />
       			<button class="button" type="submit">
          			 Edit Comment
        		</button>
    		</form>
<?php
			}
		}
	}

	function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

	function isValidPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return false;
		}
		if($number > 0)
			return TRUE;
		return FALSE;
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>