<?php
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}
	
	function changepassword($username, $newpassword) {
		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $newpassword, $username);
		if (!$stmt->execute()) 
			return FALSE;
		return TRUE;
  	}

?>