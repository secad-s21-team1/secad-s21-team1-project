<?php
	require 'database.php';
	require 'session_auth.php';

	$commentID = $_POST["commentid"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($commentID) || !isset($commentID)) {
		echo "<script>alert('Error: There is not enough information to delete this comment.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	if (strcmp(getOwnerOfComment($commentID), $_SESSION['username']) == 0) {

		if(deleteComment($commentID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot delete this comment.');</script>";
			header("Refresh:0 url=mainpage.php");
		}

	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot delete this comment.');</script>";
		exit();
	}

	function deleteComment($commentID) {
		global $mysqli;
		$prepared_sql = "DELETE FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $commentID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
	}

	function getOwnerOfComment($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>