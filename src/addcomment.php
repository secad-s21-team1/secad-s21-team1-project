<?php
	require 'database.php';
	require 'session_auth.php';

	$content = sanitize_input($_POST["content"]);
	$username = sanitize_input($_SESSION["username"]);
	$parentPostID = sanitize_input($_POST["parentpostid"]); 
	$commentID = rand(1, 999999);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	// if we have values, we try to add the comment
	if (!empty($content) AND !empty($username) AND !empty($parentPostID) AND isset($content) AND isset($username)
		AND isset($parentPostID)) {
			if(addcomment($content, $username, $parentPostID, $commentID)) {
				header("Refresh:0 url=mainpage.php");
			} else {
				echo "<script>alert('Error: Cannot create this post.');</script>";
				header("Refresh:0 url=mainpage.php");
			}
	} else {
		echo "Not enough information provided to create a post.";
		exit();
	}

	function addcomment($content, $username, $parentPostID, $commentID) {
		global $mysqli;
		$prepared_sql = "INSERT INTO comments VALUES (?, ?, ?, ?);";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("siis", $username, $commentID, $parentPostID, $content); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	  function createCommentID(){
  		$potentialcommentID = createCommentID();
  		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $potentialcommentID);
		if (!$stmt->execute()) { 
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($number)) echo "Binding failed";
		if($stmt->fetch()){
			if($number == 0){
				return $potentialcommentID;
			}
		} else {
			return createCommentID();
		}
  	}

  	// makes some content odd, but we must avoid injected code
  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}

?>