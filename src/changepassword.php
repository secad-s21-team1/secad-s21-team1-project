<?php
	require 'session_auth.php';
	require 'database.php';

	$username = sanitize_input($_SESSION["username"]);
	$newpassword = sanitize_input($_REQUEST["newpassword"]);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}
	
	if (isset($username) AND isset($newpassword)) {
		if(changepassword($username, $newpassword)) {
			echo "<h4>Password is changed successfully!.</h4>";
		} else {
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	} else {
		echo "No provided username/password to change.";
		exit();
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
