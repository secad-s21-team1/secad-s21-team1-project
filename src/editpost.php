<?php
	require 'database.php';
	require 'session_auth.php';

	$postID = $_POST["postid"];
	$content = $_POST["content"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($postID) || empty($content) || !isset($postID) || !isset($content)) {
		echo "Not enough information provided to create a post.";
		header("Refresh:0 url=mainpage.php");
	}
		
	if(strcmp(getOwnerOfPost($postID), $_SESSION['username']) == 0) {
		if(editPost($content, $postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot edit this comment.');</script>";
			header("Refresh:0 url=mainpage.php");
		}
	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot edit this comment.');</script>";
		header("Refresh:0 url=mainpage.php");
	}


	function editPost($content, $postID) {
		global $mysqli;
		$prepared_sql = "UPDATE posts SET content=? WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("si", $content, $postID); // i binds integers?
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		return TRUE;
  	}

  	  function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>