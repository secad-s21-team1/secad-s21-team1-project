<?php
	require 'database.php';
	require 'session_auth.php';

	$rand= bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	$commentid = $_POST["commentid"];


	// check if the post ID is valid
	if(!isValidPost($commentid)){
		echo "<script>alert('This comment does not exist. Please enter a valid comment ID.');</script>";
		header("Refresh:0; url=mainpage.php");
	}

	if(strcmp(getOwnerOfComment($commentid), $_SESSION["username"]) !== 0) {
		echo "<script>alert('You are not the owner of this comment - you may not edit it.');</script>";
		header("Refresh:0; url=mainpage.php");
	}

	getComment($_POST["commentid"]);
	
	if (empty($commentid) AND !isset($commentid)) {	
		echo "<script>alert('You have not selected a comment to edit.');</script>";
		header("Refresh:0; url=mainpage.php");
	}


?>

	<form action="editcomment.php" method="POST">
        Edit your comment in the box below <br>
        <input type="hidden" name="commentid" value="<?php echo  $commentid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
                    pattern=".{1,250}"
                    title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Edit Comment
        </button>
    </form>


    <form action="deletecomment.php" method="POST">
        <input type="hidden" name="commentid" value="<?php echo $commentid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
       	<button class="button" type="submit">
           Delete Comment
        </button>
    </form>

<a href="mainpage.php">Main Page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php
	function getComment($commentid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL; $content = NULL;
		if(!$stmt->bind_result($owner, $content)) echo "Binding failed";
		while($stmt->fetch()){
			echo "<br>Comment by " . htmlentities($owner) . ": " . htmlentities($content) . "<br><br><br>";
		}
	}

	function getOwnerOfComment($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

	function isValidPost($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($number)) echo "Binding failed";
		if($stmt->fetch()){
			if($number > 0){
				return TRUE;
			}
		} else {
			return false;
		}
		return FALSE;
	}
?>