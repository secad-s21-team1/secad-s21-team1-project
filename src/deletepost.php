<?php
	require 'database.php';
	require 'session_auth.php';

	$postID = $_POST["postid"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($postID) || !isset($postID)) {
		echo "<script>alert('Error: There is not enough information to make this post.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	if(strcmp(getOwnerOfPost($postID), $_SESSION["username"]) == 0) {

		if(deletePost($postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=mainpage.php");
		}

		if(deleteCommentsOfPost($postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			header("Refresh:0 url=mainpage.php");
		}

	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot delete this post.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	function deletePost($postID) {
		global $mysqli;
		$prepared_sql = "DELETE FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $postID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function deleteCommentsOfPost($postID) {
  		global $mysqli;
		$prepared_sql = "DELETE * FROM comments WHERE parentPostID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $postID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>