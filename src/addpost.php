<?php
	require 'database.php';
	require 'session_auth.php';
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	$content = sanitize_input($_POST["content"]);
	$username = sanitize_input($_SESSION["username"]);
	$postID = createPostID();


	if (!empty($content) AND !empty($username) AND isset($content) AND isset($username)) {
			if(addpost($content, $username, $postID)) {
				header("Refresh:0 url=mainpage.php");
			} else {
				echo "<script>alert('Error: Cannot create this post.');</script>";
				header("Refresh:0 url=mainpage.php");
			}
	} else {
		echo "Not enough information provided to create a post.";
		exit();
	}

	function addpost($content, $username, $postID) {
		global $mysqli;
		$prepared_sql = "INSERT INTO posts VALUES (?, ?, ?);";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sis", $username, $postID, $content); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function createPostID(){
  		$potentialpostID = rand(1, 999999);
  		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $potentialpostID);
		if (!$stmt->execute()) { 
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($number)) echo "Binding failed";
		if($stmt->fetch()){
			if($number == 0){
				return $potentialpostID;
			}
		} else {
			return createPostID();
		}
  	}

  	// makes some content looks weird, but we must prevent injected code attacks
  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}

?>