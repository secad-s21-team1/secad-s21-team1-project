<?php
	require 'database.php';

	$firstname = sanitize_input($_POST["firstname"]);
	$lastname = sanitize_input($_POST["lastname"]);
	$email = sanitize_input($_POST["email"]);
	$username = sanitize_input($_POST["username"]);
	$password = sanitize_input($_POST["password"]);

	// if we have all the values, try to add the user
	if (!empty($firstname) AND !empty($lastname) AND !empty($email) AND !empty($username) AND !empty($password) AND isset($firstname) AND isset($lastname) AND isset($email) AND isset($username) AND isset($password)) {
			if(addnewuser($firstname, $lastname, $email, $username, $password)) {
				echo "<h4>User successfully created!</h4>";
			} else {
				echo "<h4>Error: Cannot create this user.</h4>";
			}
	} else {
		echo "Not enough information provided to create a user.";
		exit();
	}

	function addnewuser($firstname, $lastname, $email, $username, $password) {
		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, ?, ?, ?, password(?));";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sssss", $firstname, $lastname, $email, $username, $password);
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>

<a href="index.php">Home</a> | <a href="logout.php">Logout</a>