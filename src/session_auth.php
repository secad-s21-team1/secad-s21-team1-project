<?php
    $lifetime = 15 * 60;
    $path = "/teamproject";
    $domain = "secad-team1-wardc11.minifacebook.com"; //change to your IP address
    $secure = TRUE;
    $httponly = TRUE;
    session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
    session_start();

    //check the session
    if(!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
    //the session is not authenticated
      echo "<script>alert('You have to login first! session_auth.php');</script>";
      session_destroy();
      header("Refresh:0; url=form.php");
      die();
    }

    if( $_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
    //it is a session hijacking attack since it comes from a different browser
      echo "<script>alert('Session hijacking attack is detected! (in session auth)');</script>";
      session_destroy();
      header("Refresh:0; url=form.php");
      die();
    }
?>