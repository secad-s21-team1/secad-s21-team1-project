<?php
	require 'database.php';
	require 'session_auth.php';
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

?>
	<h2> Hello, <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>
	<!-- CODE FOR POSTS -->
	<form action="addpost.php" method="POST">
		<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        Write a post in the box below <br>
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
            pattern=".{1,250}"
            title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Post
        </button>
    </form>

<?php
	global $mysqli;
	$prepared_sql = "SELECT owner, content, postID FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$owner = NULL; $content = NULL; $postid = NULL;
	if(!$stmt->bind_result($owner, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by '" . htmlentities($owner) . "' with Post ID " . htmlentities($postid) . ": " . htmlentities($content);
				?>
    <form action="post.php" method="POST">
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
           Go to Post
        </button>
    </form>
<?php
		echo "<br><br><br>";
	}

?>
	<a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
