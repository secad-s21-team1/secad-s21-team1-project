<?php
	$lifetime = 15 * 60;
	$path = "/teamproject/admin";	// not sure if this is right
	$domain = "secad-team1-wardc11.minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	//var_dump($_SESSION);
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			$_SESSION["role"] = "superuser"; // different from regular index.php
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	
	if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>


	<a href="mainpage.php">Main Page</a> | <a href="displayregisteredusers.php">View registered users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php		
	}else{
		echo "<script>alert('Invalid username/password');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

	// superuser use a different table to login
	function securechecklogin($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * FROM superusers WHERE user=? AND password=password(?)";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		//return TRUE;
		return FALSE; 
  	}
?>