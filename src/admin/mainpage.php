<?php
	require 'database.php';
	require 'session_auth.php';

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}  

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

?>
	<h2> Hello, <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>
	<!-- CODE FOR POSTS -->
	<form action="addpost.php" method="POST">
        Write a post in the box below <br>
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
            pattern=".{1,250}"
            title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Post
        </button>
    </form>

    <form action="post.php" method="POST">
        Input a specific post ID <br>
        <input type="number" name="postid" min="1" max = "99999999" /> <br>
       	<button class="button" type="submit">
           Go To Post
        </button>
    </form> 


<?php
	global $mysqli;
	$prepared_sql = "SELECT owner, content, postID FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$owner = NULL; $content = NULL; $postid = NULL;
	if(!$stmt->bind_result($owner, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by " . htmlentities($owner) . " with Post ID " . htmlentities($postid) . ": " . htmlentities($content) . "\n" . "<br><br><br>";
	}

?>
	<a href="displayregisteredusers.php">Display registered users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
