<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - Mick and Asia MiniFacebook</title>
</head>
<body>
      	<h1>Sign up for a new account</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login">
                First name: <input type="text" class="text_field" name="firstname" required
                    pattern="[A-Za-z]{1,20}"
                    title="Please enter a valid first name"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Last name: <input type="text" class="text_field" name="lastname" required
                    pattern="[A-Za-z]{1,20}"
                    title="Please enter a valid last name"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Email: <input type="email" class="text_field" name="email" required
                    pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                    title="Please enter a valid email"
                    placeholder="Your email address"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Username: <input type="text" class="text_field" name="username" required
                    pattern="[a-z0-9]{1,20}"
                    title="Username must have at least 4 characters. May contain: letters, numbers, _"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Password:  <input type="password" class="text_field" name="password" required
                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#%^&])[\w!@#%^&]{8,20}"
                    title="Password must have between 8-20 characters and include 1 special symbol !@#%^&, 1 number, 1 lowercase and 1 uppercase letter"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;" /> 
                    Retype password: <input type="password" class="text_field" name="repassword"
                    placeholder="Retype your password" required
                    title="Passwords do not match"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title '');" /><br>
                <button class="button" type="submit">
                  Sign up
                </button>
          </form>

</body>
</html>

