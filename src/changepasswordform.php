<?php
  require 'session_auth.php';
  $rand= bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
        <h1>Change Your Password, Team 1 Minifacebook</h1>
        <h2>By Asia and Mick</h2>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa");
?>
          <form action="changepassword.php" method="POST" class="form login">
                Username: <!--input type="text" class="text_field" name="username" /-->
                <?php echo htmlentities($_SESSION["username"]); ?>
                <br>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                New Password:  <input type="password" class="text_field" name="newpassword" required
                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&])[\w!@#$%^&]{8,20}"
                    title="Password must have between 8-20 characters and include 1 special symbol !@#$%^&, 1 number, 1 lowercase and 1 uppercase letter"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;" /> 
                    Retype password: <input type="password" class="text_field" name="repassword"
                    placeholder="Retype your password" required
                    title="Passwords do not match"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title '');" /><br>
                <button class="button" type="submit">
                  Change password
                </button>
          </form>
</body>
</html>

