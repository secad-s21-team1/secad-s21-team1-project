-- if the table exists, delete it
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `posts`;
DROP TABLE IF EXISTS `comments`;
DROP TABLE IF EXISTS `superusers`;

-- table of users
CREATE TABLE users(
	firstname varchar(20) NOT NULL,
	lastname varchar(20) NOT NULL,
	email varchar(30) NOT NULL,
	username varchar(20) PRIMARY KEY,
	password varchar(50) NOT NULL); -- we only allow length of 20 but the hash is longer

-- table of posts
CREATE TABLE posts(
	owner varchar(20),
	postID int PRIMARY KEY,
	content text NOT NULL
	-- , FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE
	);

-- table of comments under a post
CREATE TABLE comments(
	owner varchar(20),
	commentID int PRIMARY KEY,
	parentPostID int NOT NULL,
	-- add date/time
	content text NOT NULL
	-- , FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE
	);

-- table of superusers (users who can edit anything)
CREATE TABLE superusers(
	user varchar(20) PRIMARY KEY,
	password varchar(50) NOT NULL
	);

-- insert data to the table users
LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES ('User', 'User', 'user@gmail.com', 'testuser', password('testPass'));
UNLOCK TABLES;

LOCK TABLES `superusers` WRITE;
INSERT INTO `superusers` VALUES ('admin', password('testPass'));
UNLOCK TABLES;