### CPS 475/575 Secure Application Development 

# README


CPS 475 Secure Application Development

Dr. Phung

Mick Ward - wardc11 - 101608463

Asia Solomianko - solomiankoj1 - 101610041

#Mini Facebook


# 1. Introduction

The goal of this project is to create a typical social media platfrom, and implement the secure programming techniques that we have learned throughout the semester. The goal is to create a website in which users can register for an account, log in, create posts, and chat with other people in the typical way they would on facebook
Link: https://bitbucket.org/secad-s21-team1/secad-s21-team1-project/src/master/

# 2. Design

*   Users can either log in or create an account at "https://secad-team1-solomiankoj1.minifacebook.com/teamproject/form.php"
*	Users can register for an account
*   Users can log in securely
*   Users have session role "user" and superuser has session role "superuser" to distinct the types of priviledges
*   Once logged in, users have the option to go to the main page, change their password, or logout
*	Implemented tables in our database for users, posts, comments, and superusers
*	On the main page, the user can view and create posts
*   User can type in a post ID and go to the post, which will allow them to post comments or edit or delete the post if they are the owner
*   User can edit or delete their own comment
*   Superusers are already in the database - someone cannot sign up as a superuser
*   Superusers sign in at "https://secad-team1-solomiankoj1.minifacebook.com/teamproject/admin/form.php"
*   Superusers can view and edit ANY post as well as see a list of the registered users
*   User cannot log into superuser login page


# 3. Implementation & security analysis

*   System is deployed on HTTPS to have passing of encrypted data
*   Prevention of CSRF attacks by checking if the random token sent over from the request is the same as the token in the session
*   Prevention of session hijacking attacks by making sure the session browser stored is the same as the server's user agent stored
*   Prevention of SQL injection attacks by having prepared statements
*   Prevention of cross-site scripting attacks by sanitizing the code to make sure no code is executed when getting user input
*	Password is masked when logging in, creating a new account, or changing password
*   Using non-root priviledges when accessing database
*	Passwords are hashed before being stored in the database
*	Non-logged in users are always directed to the log in page
*   User cannot login at the superuser page
*   User can only edit and delete his own posts by checking the owner and session role
*   Checks to see if each post and comment ID is unique and generates unique post and comment IDs
*   Validates input of post and comment ID to make sure it exists


# 4. Demo (screenshots)

*   Everyone can register a new account and then login

![demo](/demoimages/registrationdemo.png)
![demo](/demoimages/registrationdemo2.png)

*	Logged in users may change their password

![demo](/demoimages/passwordchange.png)

*	Images of datatables in the databse

![demo](/demoimages/tables.png)

* Image of the mainpage. Here users can view posts. They can make posts, and they can enter a post id to view it. *Revised now users only need to click a bx next to posts

![demo](/demoimages/mainpage.png)

* Image of a posts page. Here viewers can edit their post, create a comment, or edit one of their comments.

![demo](/demoimages/Post.png)

* This is the image of the posts page when a user does not own the post. There is no option to delete or edit their post, only the ability to edit a comment by inputting its ID *Re

![demo](/demoimages/cantdelete.png)

* This is what happens when you try to edit a comment that is not yours

![demo](/demoimages/canteditcomment.png)

* This image shows the login screen for the Super User log in. Note the admin extension in the url.

![demo](/demoimages/superuserlogin.png)

* This image shows the screen for when the super user displays all of the registered users

![demo](/demoimages/DisplayRegisteredUsers.png)

* This image shows what happens when a regular logged in user uses the link to display users like a super user would. Because the log in for the /admin is handled differently, it will ask the user to log in to a super user account

![demo](/demoimages/adminaccessonly.png)

# Appendix


## /changepassword.php
```php
<?php
	require 'session_auth.php';
	require 'database.php';

	$username = sanitize_input($_SESSION["username"]);
	$newpassword = sanitize_input($_REQUEST["newpassword"]);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}
	
	if (isset($username) AND isset($newpassword)) {
		if(changepassword($username, $newpassword)) {
			echo "<h4>Password is changed successfully!.</h4>";
		} else {
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	} else {
		echo "No provided username/password to change.";
		exit();
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>

```
## /form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>A Simple login form, SecAD</h1>
        <h2>Team 1: Mick Ward and Asia Solomianko</h2>


<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>
          <br>
          <form action="registrationform.php">
                <button class="button" type="submit">
                  Sign up
                </button>
          </form>

</body>
</html>

## /changepassword.php
```php
<?php
	require 'session_auth.php';
	require 'database.php';

	$username = sanitize_input($_SESSION["username"]);
	$newpassword = sanitize_input($_REQUEST["newpassword"]);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}
	
	if (isset($username) AND isset($newpassword)) {
		if(changepassword($username, $newpassword)) {
			echo "<h4>Password is changed successfully!.</h4>";
		} else {
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	} else {
		echo "No provided username/password to change.";
		exit();
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>

```
## /form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>A Simple login form, SecAD</h1>
        <h2>Team 1: Mick Ward and Asia Solomianko</h2>


<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>
          <br>
          <form action="registrationform.php">
                <button class="button" type="submit">
                  Sign up
                </button>
          </form>

</body>
</html>


```
## /editpost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$postID = $_POST["postid"];
	$content = $_POST["content"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}


	if (!empty($postID) && !empty($content) && isset($postID) && isset($content) && getOwnerOfPost($postID)) {
			if(editPost($content, $postID)) {
				header("Refresh:0 url=mainpage.php");
			} else {
				echo "<script>alert('Error: Cannot edit this comment.');</script>";
				header("Refresh:0 url=mainpage.php");
			}
	} else {
		echo "Not enough information provided to create a post.";
		exit();
	}

	function editPost($content, $postID) {
		global $mysqli;
		$prepared_sql = "UPDATE posts SET content=? WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("si", $content, $postID); // i binds integers?
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		return TRUE;
  	}

  	  function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>
```
## /logout.php
```php
<?php
  session_start();
  session_destroy();
?>

<p> You are logged out! </p>

<a href="form.php">Login again</a>
```
## /deletepost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$postID = $_POST["postid"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (!empty($postID) && isset($postID) && strcmp(getOwnerOfPost($postID), $_SESSION["username"]) == 0) {

		if(deletePost($postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=mainpage.php");
		}

		if(deleteCommentsOfPost($postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			header("Refresh:0 url=mainpage.php");
		}

	} else {
		echo "Not enough information provided to delete this post.";
		exit();
	}

	function deletePost($postID) {
		global $mysqli;
		$prepared_sql = "DELETE FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $postID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function deleteCommentsOfPost($postID) {
  		global $mysqli;
		$prepared_sql = "DELETE * FROM comments WHERE parentPostID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $postID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>
```
## /changepasswordform.php
```php
<?php
  require 'session_auth.php';
  $rand= bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
        <h1>Change Your Password, Team 1 Minifacebook</h1>
        <h2>By Asia and Mick</h2>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa");
?>
          <form action="changepassword.php" method="POST" class="form login">
                Username: <!--input type="text" class="text_field" name="username" /-->
                <?php echo htmlentities($_SESSION["username"]); ?>
                <br>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                New Password:  <input type="password" class="text_field" name="newpassword" required
                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&])[\w!@#$%^&]{8,20}"
                    title="Password must have between 8-20 characters and include 1 special symbol !@#$%^&, 1 number, 1 lowercase and 1 uppercase letter"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;" /> 
                    Retype password: <input type="password" class="text_field" name="repassword"
                    placeholder="Retype your password" required
                    title="Passwords do not match"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title '');" /><br>
                <button class="button" type="submit">
                  Change password
                </button>
          </form>
</body>
</html>


```
## /index.php
```php
<?php
	$lifetime = 15 * 60;
	$path = "/teamproject";	// not sure if this is right
	$domain = "secad-team1-wardc11.miniFacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	//var_dump($_SESSION);
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
						$_SESSION["role"] = "user"; // different from admin index.php
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please login.');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

	
	if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>

	<a href="mainpage.php">Main Page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php		
	}else{
		echo "<script>alert('Invalid username/password');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

	function securechecklogin($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * FROM users WHERE username= ? AND password = password(?)";
		//echo "DEBUG > sql= $prepared_sql"; 
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		//return TRUE;
		return FALSE; 
  	}
?>
```
# Source code to Markdown
This file is automatically created by a script. Please delete this line and replace with the course and your team information accordingly.
## /changepassword.php
```php
<?php
	require 'session_auth.php';
	require 'database.php';

	$username = sanitize_input($_SESSION["username"]);
	$newpassword = sanitize_input($_REQUEST["newpassword"]);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}
	
	if (isset($username) AND isset($newpassword)) {
		if(changepassword($username, $newpassword)) {
			echo "<h4>Password is changed successfully!.</h4>";
		} else {
			echo "<h4>Error: Cannot change the password.</h4>";
		}
	} else {
		echo "No provided username/password to change.";
		exit();
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
<a href="index.php">Home</a> | <a href="logout.php">Logout</a>

```
## /form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>A Simple login form, SecAD</h1>
        <h2>Team 1: Mick Ward and Asia Solomianko</h2>


<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>
          <br>
          <form action="registrationform.php">
                <button class="button" type="submit">
                  Sign up
                </button>
          </form>

</body>
</html>


```
## /editpost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$postID = $_POST["postid"];
	$content = $_POST["content"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($postID) || empty($content) || !isset($postID) || !isset($content)) {
		echo "Not enough information provided to create a post.";
		header("Refresh:0 url=mainpage.php");
	}
		
	if(strcmp(getOwnerOfPost($postID), $_SESSION['username']) == 0) {
		if(editPost($content, $postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot edit this comment.');</script>";
			header("Refresh:0 url=mainpage.php");
		}
	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot edit this comment.');</script>";
		header("Refresh:0 url=mainpage.php");
	}


	function editPost($content, $postID) {
		global $mysqli;
		$prepared_sql = "UPDATE posts SET content=? WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("si", $content, $postID); // i binds integers?
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		return TRUE;
  	}

  	  function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>
```
## /logout.php
```php
<?php
  session_start();
  session_destroy();
?>

<p> You are logged out! </p>

<a href="form.php">Login again</a>
```
## /deletepost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$postID = $_POST["postid"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($postID) || !isset($postID)) {
		echo "<script>alert('Error: There is not enough information to make this post.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	if(strcmp(getOwnerOfPost($postID), $_SESSION["username"]) == 0) {

		if(deletePost($postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot delete this post.');</script>";
			header("Refresh:0 url=mainpage.php");
		}

		if(deleteCommentsOfPost($postID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			header("Refresh:0 url=mainpage.php");
		}

	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot delete this post.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	function deletePost($postID) {
		global $mysqli;
		$prepared_sql = "DELETE FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $postID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function deleteCommentsOfPost($postID) {
  		global $mysqli;
		$prepared_sql = "DELETE * FROM comments WHERE parentPostID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $postID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>
```
## /changepasswordform.php
```php
<?php
  require 'session_auth.php';
  $rand= bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
        <h1>Change Your Password, Team 1 Minifacebook</h1>
        <h2>By Asia and Mick</h2>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa");
?>
          <form action="changepassword.php" method="POST" class="form login">
                Username: <!--input type="text" class="text_field" name="username" /-->
                <?php echo htmlentities($_SESSION["username"]); ?>
                <br>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                New Password:  <input type="password" class="text_field" name="newpassword" required
                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&])[\w!@#$%^&]{8,20}"
                    title="Password must have between 8-20 characters and include 1 special symbol !@#$%^&, 1 number, 1 lowercase and 1 uppercase letter"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;" /> 
                    Retype password: <input type="password" class="text_field" name="repassword"
                    placeholder="Retype your password" required
                    title="Passwords do not match"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title '');" /><br>
                <button class="button" type="submit">
                  Change password
                </button>
          </form>
</body>
</html>


```
## /index.php
```php
<?php
	$lifetime = 15 * 60;
	$path = "/teamproject";	// not sure if this is right
	$domain = "secad-team1-wardc11.miniFacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	//var_dump($_SESSION);
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
						$_SESSION["role"] = "user"; // different from admin index.php
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
		echo "<script>alert('You have not logged in. Please login.');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]) {
		echo "<script>alert('Session hijacking attack is detected!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

	
	if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>

	<a href="mainpage.php">Main Page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php		
	}else{
		echo "<script>alert('Invalid username/password');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

	function securechecklogin($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * FROM users WHERE username= ? AND password = password(?)";
		//echo "DEBUG > sql= $prepared_sql"; 
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		//return TRUE;
		return FALSE; 
  	}
?>
```
## /admin/form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>Superuser login form, SecAD</h1>
        <h2>Team 1: Mick Ward and Asia Solomianko</h2>


<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>
          <br>

</body>
</html>


```
## /admin/displayregisteredusers.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	// DO WE NEED THESE TWO ABOVE?

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}


?>
	<h2> Please view the registered users below:</h2>
	<br><br> 

<?php
	// show all the regular users
	global $mysqli;
	$prepared_sql = "SELECT firstname, lastname, email, username FROM users;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$fname = NULL; $lname = NULL; $email = NULL; $username = NULL;
	if(!$stmt->bind_result($fname, $lname, $email, $username)) echo "Binding failed";
	while($stmt->fetch()){
		echo htmlentities($fname) . " " . htmlentities($lname) . ", " . htmlentities($email) . ", username '" . htmlentities($username) . "'<br>";
	}

	echo "<br><br>";

	// show all the superusers
	$prepared_sql = "SELECT user FROM superusers;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Superuser with username '" . htmlentities($username) . "'<br>";
	}
?>
	<br>
	<a href="mainpage.php">Go back to the main page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

```
## /admin/index.php
```php
<?php
	$lifetime = 15 * 60;
	$path = "/teamproject/admin";	// not sure if this is right
	$domain = "secad-team1-wardc11.minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	//var_dump($_SESSION);
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			$_SESSION["role"] = "superuser"; // different from regular index.php
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	
	if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>


	<a href="mainpage.php">Main Page</a> | <a href="displayregisteredusers.php">View registered users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php		
	}else{
		echo "<script>alert('Invalid username/password');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

	// superuser use a different table to login
	function securechecklogin($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * FROM superusers WHERE user=? AND password=password(?)";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		//return TRUE;
		return FALSE; 
  	}
?>
```
## /admin/mainpage.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}  

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

?>
	<h2> Hello, <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>
	<!-- CODE FOR POSTS -->
	<form action="addpost.php" method="POST">
        Write a post in the box below <br>
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
            pattern=".{1,250}"
            title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Post
        </button>
    </form>

    <form action="post.php" method="POST">
        Input a specific post ID <br>
        <input type="number" name="postid" min="1" max = "99999999" /> <br>
       	<button class="button" type="submit">
           Go To Post
        </button>
    </form> 


<?php
	global $mysqli;
	$prepared_sql = "SELECT owner, content, postID FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$owner = NULL; $content = NULL; $postid = NULL;
	if(!$stmt->bind_result($owner, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by " . htmlentities($owner) . " with Post ID " . htmlentities($postid) . ": " . htmlentities($content) . "\n" . "<br><br><br>";
	}

?>
	<a href="displayregisteredusers.php">Display registered users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

```
## /addcomment.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$content = sanitize_input($_POST["content"]);
	$username = sanitize_input($_SESSION["username"]);
	$parentPostID = sanitize_input($_POST["parentpostid"]); 
	$commentID = rand(1, 999999);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	// if we have values, we try to add the comment
	if (!empty($content) AND !empty($username) AND !empty($parentPostID) AND isset($content) AND isset($username)
		AND isset($parentPostID)) {
			if(addcomment($content, $username, $parentPostID, $commentID)) {
				header("Refresh:0 url=mainpage.php");
			} else {
				echo "<script>alert('Error: Cannot create this post.');</script>";
				header("Refresh:0 url=mainpage.php");
			}
	} else {
		echo "Not enough information provided to create a post.";
		exit();
	}

	function addcomment($content, $username, $parentPostID, $commentID) {
		global $mysqli;
		$prepared_sql = "INSERT INTO comments VALUES (?, ?, ?, ?);";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("siis", $username, $commentID, $parentPostID, $content); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	  function createCommentID(){
  		$potentialcommentID = createCommentID();
  		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $potentialcommentID);
		if (!$stmt->execute()) { 
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($number)) echo "Binding failed";
		if($stmt->fetch()){
			if($number == 0){
				return $potentialcommentID;
			}
		} else {
			return createCommentID();
		}
  	}

  	// makes some content odd, but we must avoid injected code
  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}

?>
```
## /mainpage.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

?>
	<h2> Hello, <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>
	<!-- CODE FOR POSTS -->
	<form action="addpost.php" method="POST">
		<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        Write a post in the box below <br>
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
            pattern=".{1,250}"
            title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Post
        </button>
    </form>

<?php
	global $mysqli;
	$prepared_sql = "SELECT owner, content, postID FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$owner = NULL; $content = NULL; $postid = NULL;
	if(!$stmt->bind_result($owner, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by '" . htmlentities($owner) . "' with Post ID " . htmlentities($postid) . ": " . htmlentities($content);
				?>
    <form action="post.php" method="POST">
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
       	<button class="button" type="submit">
           Go to Post
        </button>
    </form>
<?php
		echo "<br><br><br>";
	}

?>
	<a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

```
## /session_auth.php
```php
<?php
    $lifetime = 15 * 60;
    $path = "/teamproject";
    $domain = "secad-team1-wardc11.minifacebook.com"; //change to your IP address
    $secure = TRUE;
    $httponly = TRUE;
    session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
    session_start();

    //check the session
    if(!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
    //the session is not authenticated
      echo "<script>alert('You have to login first! session_auth.php');</script>";
      session_destroy();
      header("Refresh:0; url=form.php");
      die();
    }

    if( $_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
    //it is a session hijacking attack since it comes from a different browser
      echo "<script>alert('Session hijacking attack is detected! (in session auth)');</script>";
      session_destroy();
      header("Refresh:0; url=form.php");
      die();
    }
?>
```
## /database.php
```php
<?php
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}
	
	function changepassword($username, $newpassword) {
		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("ss", $newpassword, $username);
		if (!$stmt->execute()) 
			return FALSE;
		return TRUE;
  	}

?>
```
## /post.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	$rand= bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}

	$postid = sanitize_input($_POST["postid"]);

	// check if the post ID is valid
	if(!isValidPost($postid)) {
		echo "<script>alert('This post does not exist. Please enter a valid post ID.');</script>";
		header("Refresh:0; url=mainpage.php");
	}

	getPost($_POST["postid"]);
	echo "Comment Section: \r\n"; // what is \r\n?
	echo "<br><br>";
	showComments($_POST["postid"]);


	if(strcmp(getOwnerOfPost($postid), $_SESSION["username"]) == 0) {
?>
		<form action="editpost.php" method="POST">
	        Edit your post in the box below <br>
	        <input type="hidden" name="postid" value="<?php echo  $postid; ?>" />
	        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
	        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
	                    pattern=".{1,250}"
	                    title="The post must have between 1 and 250 characters" /> <br>
	       	<button class="button" type="submit">
	           Edit Post
	        </button>
	    </form>
<?php
	}
?>

	<form action="addcomment.php" method="POST">
        Write a comment in the box below <br>
        <input type="hidden" name="parentpostid" value="<?php echo  $postid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
                    pattern=".{1,250}"
                    title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Comment
        </button>
    </form>


<?php
	// user can delete post if he is the owner or a superuser
	if(strcmp(getOwnerOfPost($postid), $_SESSION["username"]) == 0) {
?>
    <form action="deletepost.php" method="POST">
        <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
       	<button class="button" type="submit">
           Delete Post
        </button>
    </form>
<?php
	}
?>

<a href="mainpage.php">Main Page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php
	function getPost($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content FROM posts WHERE postid=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL; $content = NULL;
		if(!$stmt->bind_result($owner, $content)) echo "Binding failed";
		while($stmt->fetch()){
			echo "<br>Post by " . htmlentities($owner) . ": " . htmlentities($content) . "<br><br><br>";
		}
	}

	function showComments($postid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content, commentID FROM comments WHERE parentPostID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL; $content = NULL; $commentID = NULL;
		if(!$stmt->bind_result($owner, $content, $commentID)) echo "Binding failed";
		while($stmt->fetch()){
			echo htmlentities($owner) . " with comment ID " . htmlentities($commentID) . ": " . htmlentities($content) . "<br><br>";
			if (strcmp($owner, $_SESSION['username']) == 0){

	?>
    		<form action="comment.php" method="POST">
        		<input type="hidden" name="commentid" value="<?php echo $commentID; ?>" />
       			<button class="button" type="submit">
          			 Edit Comment
        		</button>
    		</form>
<?php
			}
		}
	}

	function getOwnerOfPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

	function isValidPost($postid) {
		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $postid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return false;
		}
		if($number > 0)
			return TRUE;
		return FALSE;
	}

	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>
```
## /deletecomment.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$commentID = $_POST["commentid"];
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($commentID) || !isset($commentID)) {
		echo "<script>alert('Error: There is not enough information to delete this comment.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	if (strcmp(getOwnerOfComment($commentID), $_SESSION['username']) == 0) {

		if(deleteComment($commentID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot delete this comment.');</script>";
			header("Refresh:0 url=mainpage.php");
		}

	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot delete this comment.');</script>";
		exit();
	}

	function deleteComment($commentID) {
		global $mysqli;
		$prepared_sql = "DELETE FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("i", $commentID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
	}

	function getOwnerOfComment($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

?>
```
## /addnewuser.php
```php
<?php
	require 'database.php';

	$firstname = sanitize_input($_POST["firstname"]);
	$lastname = sanitize_input($_POST["lastname"]);
	$email = sanitize_input($_POST["email"]);
	$username = sanitize_input($_POST["username"]);
	$password = sanitize_input($_POST["password"]);

	// if we have all the values, try to add the user
	if (!empty($firstname) AND !empty($lastname) AND !empty($email) AND !empty($username) AND !empty($password) AND isset($firstname) AND isset($lastname) AND isset($email) AND isset($username) AND isset($password)) {
			if(addnewuser($firstname, $lastname, $email, $username, $password)) {
				echo "<h4>User successfully created!</h4>";
			} else {
				echo "<h4>Error: Cannot create this user.</h4>";
			}
	} else {
		echo "Not enough information provided to create a user.";
		exit();
	}

	function addnewuser($firstname, $lastname, $email, $username, $password) {
		global $mysqli;
		$prepared_sql = "INSERT INTO users VALUES (?, ?, ?, ?, password(?));";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sssss", $firstname, $lastname, $email, $username, $password);
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}
?>

<a href="index.php">Home</a> | <a href="logout.php">Logout</a>
```
## /editcomment.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$commentID = sanitize_input($_POST["commentid"]);
	$content = sanitize_input($_POST["content"]);
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	if (empty($commentID) || empty($content) || !isset($commentID) || !isset($content)) {
		echo "<script>alert('Error: There is not enough information to edit the comment.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	if (strcmp(getOwnerOfComment($commentID), $_SESSION['username']) == 0) {
		if(editComment($content, $commentID)) {
			header("Refresh:0 url=mainpage.php");
		} else {
			echo "<script>alert('Error: Cannot edit this comment.');</script>";
			header("Refresh:0 url=mainpage.php");
		}
	} else {
		echo "<script>alert('Error: You are not the owner, so you cannot edit this comment.');</script>";
		header("Refresh:0 url=mainpage.php");
	}

	function editComment($content, $commentID) {
		global $mysqli;
		$prepared_sql = "UPDATE comments SET content=? WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("si", $content, $commentID); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function getOwnerOfComment($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

  	function sanitize_input($input) {
  		$input = trim($input);
  		$input = stripslashes($input);
  		$input = htmlspecialchars($input);
  		return $input;
  	}

?>
```
## /comment.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$rand= bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	$commentid = $_POST["commentid"];


	// check if the post ID is valid
	if(!isValidPost($commentid)){
		echo "<script>alert('This comment does not exist. Please enter a valid comment ID.');</script>";
		header("Refresh:0; url=mainpage.php");
	}

	if(strcmp(getOwnerOfComment($commentid), $_SESSION["username"]) !== 0) {
		echo "<script>alert('You are not the owner of this comment - you may not edit it.');</script>";
		header("Refresh:0; url=mainpage.php");
	}

	getComment($_POST["commentid"]);
	
	if (empty($commentid) AND !isset($commentid)) {	
		echo "<script>alert('You have not selected a comment to edit.');</script>";
		header("Refresh:0; url=mainpage.php");
	}


?>

	<form action="editcomment.php" method="POST">
        Edit your comment in the box below <br>
        <input type="hidden" name="commentid" value="<?php echo  $commentid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
                    pattern=".{1,250}"
                    title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Edit Comment
        </button>
    </form>


    <form action="deletecomment.php" method="POST">
        <input type="hidden" name="commentid" value="<?php echo $commentid; ?>" />
        <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
       	<button class="button" type="submit">
           Delete Comment
        </button>
    </form>

<a href="mainpage.php">Main Page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php
	function getComment($commentid){
		global $mysqli;
		$prepared_sql = "SELECT owner, content FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL; $content = NULL;
		if(!$stmt->bind_result($owner, $content)) echo "Binding failed";
		while($stmt->fetch()){
			echo "<br>Comment by " . htmlentities($owner) . ": " . htmlentities($content) . "<br><br><br>";
		}
	}

	function getOwnerOfComment($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT owner FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
				echo "Stuck!";
				return FALSE;
			}
		$owner = NULL;
		if(!$stmt->bind_result($owner)) echo "Binding failed";
		if($stmt->fetch()){
			return htmlentities($owner);
		} else {
			return NULL;
		}
	}

	function isValidPost($commentid) {
		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM comments WHERE commentID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $commentid);
		if (!$stmt->execute()) { 
			echo "Stuck!";
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($number)) echo "Binding failed";
		if($stmt->fetch()){
			if($number > 0){
				return TRUE;
			}
		} else {
			return false;
		}
		return FALSE;
	}
?>
```
## /registrationform.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - Mick and Asia MiniFacebook</title>
</head>
<body>
      	<h1>Sign up for a new account</h1>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login">
                First name: <input type="text" class="text_field" name="firstname" required
                    pattern="[A-Za-z]{1,20}"
                    title="Please enter a valid first name"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Last name: <input type="text" class="text_field" name="lastname" required
                    pattern="[A-Za-z]{1,20}"
                    title="Please enter a valid last name"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Email: <input type="email" class="text_field" name="email" required
                    pattern="^[\w.-]+@[\w-]+(.[\w-]+)*$"
                    title="Please enter a valid email"
                    placeholder="Your email address"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Username: <input type="text" class="text_field" name="username" required
                    pattern="[a-z0-9]{1,20}"
                    title="Username must have at least 4 characters. May contain: letters, numbers, _"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');" /> <br>
                Password:  <input type="password" class="text_field" name="password" required
                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#%^&])[\w!@#%^&]{8,20}"
                    title="Password must have between 8-20 characters and include 1 special symbol !@#%^&, 1 number, 1 lowercase and 1 uppercase letter"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;" /> 
                    Retype password: <input type="password" class="text_field" name="repassword"
                    placeholder="Retype your password" required
                    title="Passwords do not match"
                    onchange="this.setCustomValidity(this.validity.patternMismatch?this.title '');" /><br>
                <button class="button" type="submit">
                  Sign up
                </button>
          </form>

</body>
</html>


```
## /addpost.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	$nocsrftoken = $_REQUEST["nocsrftoken"];

	// check for CSRF attack
	if(!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('CSRF is detected!');</script>";
		header("Refresh:0 url=logout.php");
		die();
	}

	$content = sanitize_input($_POST["content"]);
	$username = sanitize_input($_SESSION["username"]);
	$postID = createPostID();


	if (!empty($content) AND !empty($username) AND isset($content) AND isset($username)) {
			if(addpost($content, $username, $postID)) {
				header("Refresh:0 url=mainpage.php");
			} else {
				echo "<script>alert('Error: Cannot create this post.');</script>";
				header("Refresh:0 url=mainpage.php");
			}
	} else {
		echo "Not enough information provided to create a post.";
		exit();
	}

	function addpost($content, $username, $postID) {
		global $mysqli;
		$prepared_sql = "INSERT INTO posts VALUES (?, ?, ?);";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param("sis", $username, $postID, $content); // i binds integers?
		if (!$stmt->execute()) { 
			return FALSE;
		}
		return TRUE;
  	}

  	function createPostID(){
  		$potentialpostID = rand(1, 999999);
  		global $mysqli;
		$prepared_sql = "SELECT COUNT(*) FROM posts WHERE postID=?;";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			return FALSE;
		$stmt->bind_param('i', $potentialpostID);
		if (!$stmt->execute()) { 
			return FALSE;
		}
		$number = 0;
		if(!$stmt->bind_result($number)) echo "Binding failed";
		if($stmt->fetch()){
			if($number == 0){
				return $potentialpostID;
			}
		} else {
			return createPostID();
		}
  	}

  	// makes some content looks weird, but we must prevent injected code attacks
  	function sanitize_input($input) {
  		$input = htmlspecialchars($input);
  		return $input;
  	}

?>
```




## HERE IS THE CODE FOR THE FILES THAT ARE IN THE ADMIN FOLDER. SOME MAY HAVE THE SAME NAME AS PREVIOUS FILES

## /form.php
```php
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login page - SecAD</title>
</head>
<body>
      	<h1>Superuser login form, SecAD</h1>
        <h2>Team 1: Mick Ward and Asia Solomianko</h2>


<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>
          <br>

</body>
</html>


```
## /displayregisteredusers.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';
	// DO WE NEED THESE TWO ABOVE?

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}


?>
	<h2> Please view the registered users below:</h2>
	<br><br> 

<?php
	// show all the regular users
	global $mysqli;
	$prepared_sql = "SELECT firstname, lastname, email, username FROM users;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$fname = NULL; $lname = NULL; $email = NULL; $username = NULL;
	if(!$stmt->bind_result($fname, $lname, $email, $username)) echo "Binding failed";
	while($stmt->fetch()){
		echo htmlentities($fname) . " " . htmlentities($lname) . ", " . htmlentities($email) . ", username '" . htmlentities($username) . "'<br>";
	}

	echo "<br><br>";

	// show all the superusers
	$prepared_sql = "SELECT user FROM superusers;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$username = NULL;
	if(!$stmt->bind_result($username)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Superuser with username '" . htmlentities($username) . "'<br>";
	}
?>
	<br>
	<a href="mainpage.php">Go back to the main page</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

```
## /index.php
```php
<?php
	$lifetime = 15 * 60;
	$path = "/teamproject/admin";	// not sure if this is right
	$domain = "secad-team1-wardc11.minifacebook.com";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();
	//var_dump($_SESSION);
	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}    

	if (isset($_POST["username"]) and isset($_POST["password"])) { 

		if (securechecklogin($_POST["username"],$_POST["password"])) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
			$_SESSION["role"] = "superuser"; // different from regular index.php
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			session_destroy();
			header("Refresh:0; url=form.php");
			die();
		}
	}

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	
	if (securechecklogin($_POST["username"],$_POST["password"])) {
?>
	<h2> Welcome <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>


	<a href="mainpage.php">Main Page</a> | <a href="displayregisteredusers.php">View registered users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>
<?php		
	}else{
		echo "<script>alert('Invalid username/password');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

	// superuser use a different table to login
	function securechecklogin($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * FROM superusers WHERE user=? AND password=password(?)";
		if (!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss", $username, $password);
		if (!$stmt->execute()) 
			echo "Execute Error";
		if (!$stmt->store_result())
			echo "Store Result Error";
		$result = $stmt;
		if ($result->num_rows == 1)
			return TRUE;
		//return TRUE;
		return FALSE; 
  	}
?>
```
## /mainpage.php
```php
<?php
	require 'database.php';
	require 'session_auth.php';

	$mysqli = new mysqli('localhost',
			     'secad-team1',
			     'team1Pass',
			     'secad_team1');
	if ($mysqli->connect_errno) {
		printf("Database connection failed: %s\n", $mysqli->connect_errno);
		exit();
	}  

	if(strcmp($_SESSION["role"], "superuser") !== 0) {
		echo "<script>alert(You are not authorized to access this!);</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}

?>
	<h2> Hello, <?php echo htmlentities($_SESSION['username']); ?> !</h2>
	<br>
	<!-- CODE FOR POSTS -->
	<form action="addpost.php" method="POST">
        Write a post in the box below <br>
        <input type="text" class="text_field" name="content" size="50" maxlength="250" required
            pattern=".{1,250}"
            title="The post must have between 1 and 250 characters" /> <br>
       	<button class="button" type="submit">
           Add Post
        </button>
    </form>

    <form action="post.php" method="POST">
        Input a specific post ID <br>
        <input type="number" name="postid" min="1" max = "99999999" /> <br>
       	<button class="button" type="submit">
           Go To Post
        </button>
    </form> 


<?php
	global $mysqli;
	$prepared_sql = "SELECT owner, content, postID FROM posts;";
	if (!$stmt = $mysqli->prepare($prepared_sql))
		return FALSE;
	if (!$stmt->execute()) { 
		echo "Stuck!";
		return FALSE;
	}
	$owner = NULL; $content = NULL; $postid = NULL;
	if(!$stmt->bind_result($owner, $content, $postid)) echo "Binding failed";
	while($stmt->fetch()){
		echo "Post by " . htmlentities($owner) . " with Post ID " . htmlentities($postid) . ": " . htmlentities($content) . "\n" . "<br><br><br>";
	}

?>
	<a href="displayregisteredusers.php">Display registered users</a> | <a href="changepasswordform.php">Change password</a> | <a href="logout.php">Logout</a>

```

## Here is the code for database.sql

-- if the table exists, delete it

DROP TABLE IF EXISTS `users`;

DROP TABLE IF EXISTS `posts`;

DROP TABLE IF EXISTS `comments`;

DROP TABLE IF EXISTS `superusers`;

-- table of users

CREATE TABLE users(

    firstname varchar(20) NOT NULL,

    lastname varchar(20) NOT NULL,

    email varchar(30) NOT NULL,

    username varchar(20) PRIMARY KEY,

    password varchar(50) NOT NULL); -- we only allow length of 20 but the hash is longer

-- table of posts

CREATE TABLE posts(

    owner varchar(20),

    postID int PRIMARY KEY,

    content text NOT NULL

    -- , FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE

    );

-- table of comments under a post

CREATE TABLE comments(

    owner varchar(20),

    commentID int PRIMARY KEY,

    parentPostID int NOT NULL,

    -- add date/time

    content text NOT NULL

    -- , FOREIGN KEY (owner) REFERENCES users(username) ON DELETE CASCADE

    );

-- table of superusers (users who can edit anything)

CREATE TABLE superusers(

    user varchar(20) PRIMARY KEY,

    password varchar(50) NOT NULL

    );

-- insert data to the table users

LOCK TABLES `users` WRITE;

INSERT INTO `users` VALUES ('User', 'User', 'user@gmail.com', 'testuser', password('testPass'));

UNLOCK TABLES;

LOCK TABLES `superusers` WRITE;

INSERT INTO `superusers` VALUES ('admin', password('testPass'));

UNLOCK TABLES;
